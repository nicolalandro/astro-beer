[![pipeline status](https://gitlab.com/nicolalandro/astro-beer/badges/main/pipeline.svg)](https://gitlab.com/nicolalandro/astro-beer/-/commits/main)

[![gitlab pages deployed page link](https://img.shields.io/badge/Gitlab-Pages-orange?logo=gitlab)](https://nicolalandro.gitlab.io/astro-beer/)
[![vercel deployed page link](https://img.shields.io/badge/Vercel-Site-blue?logo=vercel)](https://astro-beer.vercel.app/)
[![netlify deployed page ling](https://img.shields.io/badge/Netlify-Site-blue?logo=netlify&color=0b965a)](https://wondrous-faloodeh-043e48.netlify.app/)

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/git/external?repository-url=https%3A%2F%2Fgitlab.com%2Fnicolalandro%2Fastro-beer&envDescription=Your%20Appwrite%20Endpoint%2C%20Project%20ID%20and%20Collection%20ID%20) [![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://gitlab.com/nicolalandro/astro-beer)

# Astro Beer
A simple project to use astro and svelte for list beers.

## Local usage
* install reuqirements
```
npm install
```
*  run server
```
npm run dev
# server on localhost:3000
```
* run tests
```
npx cypress run --browser firefox
npx cypress run --browser chrome
```
* open cypress ide
```
npx cypress open 
```

## References
* [Astro](https://astro.build/): Meta framework that support vuejs, react, svelte...
* [Svelte](https://svelte.dev/): Front end framework
* [Agnostic UI](https://www.agnosticui.com/): CSS framework
* [Punk API](https://punkapi.com/): a beers api used as backend
* [Axios](https://axios-http.com/docs/intro): http requets library
* [Cypress](https://docs.cypress.io/guides/getting-started/opening-the-app): testing library
  * [Network request](https://docs.cypress.io/guides/guides/network-requests): how to mock network request
* [Gitlab CI-CD](https://docs.gitlab.com/ee/ci/): to run test on each push and other CI/CD tasks
* [Gitlab Pages](https://about.gitlab.com/blog/2022/10/24/publishing-an-astro-site-with-pages/): how to deploy into gitlab pages
