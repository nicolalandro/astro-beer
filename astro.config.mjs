import { defineConfig } from 'astro/config';

// https://astro.build/config
import svelte from "@astrojs/svelte";

// https://astro.build/config
export default defineConfig({
  integrations: [svelte()],
  outDir: 'public',
  publicDir: 'static',
  base: process.env.VERCEL? '': '/astro-beer'
});