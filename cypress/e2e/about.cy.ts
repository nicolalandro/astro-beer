describe('about', () => {
  it('find lorem impsum', () => {
    cy.visit('http://localhost:3000/astro-beer/about')

    cy.contains('Lorem ipsum dolor')
  })
})