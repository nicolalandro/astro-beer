describe('homepage', () => {
  it('show test card', () => {
    cy.intercept(
      {
        method: 'GET',
        url: 'https://api.punkapi.com/v2/*',
      },
      [
        {
          name: "test",
          image_url: "https://images.punkapi.com/v2/2.png"
        }
      ]
    ).as('getBeers')

    cy.visit('http://localhost:3000/astro-beer/')

    cy.wait('@getBeers')

    cy.contains('test')
  })

  it('paginate up', () => {
    cy.intercept(
      {
        method: 'GET',
        url: 'https://api.punkapi.com/v2/beers?page=1&per_page=5',
      },
      [
        {
          name: "test",
          image_url: "https://images.punkapi.com/v2/2.png"
        }
      ]
    ).as('getBeers1')
    
    cy.intercept(
      {
        method: 'GET',
        url: 'https://api.punkapi.com/v2/beers?page=2&per_page=5',
      },
      [
        {
          name: "test 2",
          image_url: "https://images.punkapi.com/v2/2.png"
        }
      ]
    ).as('getBeers2')

    cy.visit('http://localhost:3000/astro-beer/')

    cy.wait('@getBeers1')

    cy.get('button:contains("+")').click()

    cy.wait('@getBeers2')

    cy.contains('Page 2')
  })

  it('paginate down not over the 0', () => {
    cy.intercept(
      {
        method: 'GET',
        url: 'https://api.punkapi.com/v2/*',
      },
      [
        {
          name: "test",
          image_url: "https://images.punkapi.com/v2/2.png"
        }
      ]
    ).as('getBeers')
    
    cy.visit('http://localhost:3000/astro-beer/')

    cy.wait('@getBeers')

    cy.get('button:contains("-")').click()
    
    cy.contains('Page 1')
  })
})